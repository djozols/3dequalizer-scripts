#
#
# 3DE4.script.name:	Create points for intersection
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	For use with intersect points script
#
# 3DE4.script.gui:	Manual Tracking Controls::IntersectPoints
# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
#
# David Ozols 2018
#
#
# Script to create reference points for use in intersect points
#
#TODO:
#Points not set to calculate by default - DONE
#Points be a different colour by default - DONE

import itertools

def create_intersect_ref():
	curGroup = tde4.getCurrentPGroup()
	selected_points = tde4.getPointList (curGroup , 1)
	
	if (len(selected_points) != 1):
		tde4.postQuestionRequester("Info" , "Please select a single point" , "OK")
		
	else:
		baseName = tde4.getPointName(curGroup , selected_points[0])
		
		#if each search by name for _A1 _A2 etc returns none proceed
		if (tde4.findPointByName(curGroup, baseName + "_A1") == None
		and tde4.findPointByName(curGroup, baseName + "_A2") == None
		and tde4.findPointByName(curGroup, baseName + "_B1") == None
		and tde4.findPointByName(curGroup, baseName + "_B2") == None):
		
			pointA1 = tde4.createPoint(curGroup)	#create our 4 ref points
			pointA2 = tde4.createPoint(curGroup)
			pointB1 = tde4.createPoint(curGroup)
			pointB2 = tde4.createPoint(curGroup)
			
			tde4.setPointName(curGroup , pointA1 , baseName + "_A1")	#name them
			tde4.setPointName(curGroup , pointA2 , baseName + "_A2")
			tde4.setPointName(curGroup , pointB1 , baseName + "_B1")
			tde4.setPointName(curGroup , pointB2 , baseName + "_B2")
			
			tde4.setPointCalcMode(curGroup , pointA1 , "CALC_OFF")		#ensure they aren't being calculated.
			tde4.setPointCalcMode(curGroup , pointA2 , "CALC_OFF")
			tde4.setPointCalcMode(curGroup , pointB1 , "CALC_OFF")
			tde4.setPointCalcMode(curGroup , pointB2 , "CALC_OFF")
			
			tde4.setPointColor2D(curGroup, pointA1, 11) #recolor points - no. can be changed to a value from 0-11 for a different colour
			tde4.setPointColor2D(curGroup, pointA2, 11)
			tde4.setPointColor2D(curGroup, pointB1, 11)
			tde4.setPointColor2D(curGroup, pointB2, 11)
		else:
			tde4.postQuestionRequester("Name conflict detected" , "Ensure there are no points named '<name>_A1' '_A2' '_B1' '_B2'.\nHave you already created intersect points?" , "OK")
create_intersect_ref()