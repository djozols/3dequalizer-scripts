#
#
# 3DE4.script.name:	Update point by intersection
#
# 3DE4.script.version:		v1.0
#
# 3DE4.script.comment:	Use with create points for intersection
#
# 3DE4.script.gui:	Manual Tracking Controls::IntersectPoints
# 3DE4.script.hide: false
# 3DE4.script.startup: false
#
#
# David Ozols 2018
#
#
# Script to generate a 2d position based on the intersection of two lines A and B, each defined by two points
#
#TODO:
#take into account distortion:
#	remove distortion from reference points calculate intersect, reapply distortion to intersect - DONE
#check that ref points exist on the current frame - DONE

import itertools

def update_by_intersect():

	cam = tde4.getCurrentCamera()
	curFrame = tde4.getCurrentFrame(cam)
	curGroup = tde4.getCurrentPGroup()
	selected_points = tde4.getPointList (curGroup , 1)

	if (len(selected_points) != 1):
		tde4.postQuestionRequester("Info" , "Please select a single point" , "OK")
		
	else:
	
		curPoint = selected_points[0]
		baseName = tde4.getPointName(curGroup , curPoint)
		
		if(
		tde4.findPointByName(curGroup, baseName + "_A1") == None
		or tde4.findPointByName(curGroup, baseName + "_A2") == None
		or tde4.findPointByName(curGroup, baseName + "_B1") == None
		or tde4.findPointByName(curGroup, baseName + "_B2") == None):
		
			tde4.postQuestionRequester("Info" , "Couldn't find ref points ensure they exist with correct name scheme.\nHave you used create intersect point tool yet?" , "OK")
			
		else:
			
			#look up the id of our reference points following the naming rule used in create points script

			pointA1_id = tde4.findPointByName(curGroup, baseName + "_A1") 
			pointA2_id = tde4.findPointByName(curGroup, baseName + "_A2")
			pointB1_id = tde4.findPointByName(curGroup, baseName + "_B1")
			pointB2_id = tde4.findPointByName(curGroup, baseName + "_B2")
			
			if (
			tde4.getPointStatus2D(curGroup, pointA1_id, cam, curFrame) == "POINT_UNDEFINED"
			or tde4.getPointStatus2D(curGroup, pointA2_id, cam, curFrame) == "POINT_UNDEFINED"
			or tde4.getPointStatus2D(curGroup, pointB1_id, cam, curFrame) == "POINT_UNDEFINED"
			or tde4.getPointStatus2D(curGroup, pointB2_id, cam, curFrame) == "POINT_UNDEFINED"):
				
				tde4.postQuestionRequester("Info" , "Some or all reference points are undefined for current frame." , "OK")
				
			else:
			
			#define new vectors for each point's undistorted location (makes next steps easier to read)
			
				A1 = tde4.removeDistortion2D(cam, curFrame, tde4.getPointPosition2D(curGroup, pointA1_id, cam, curFrame))
				A2 = tde4.removeDistortion2D(cam, curFrame, tde4.getPointPosition2D(curGroup, pointA2_id, cam, curFrame))
				B1 = tde4.removeDistortion2D(cam, curFrame, tde4.getPointPosition2D(curGroup, pointB1_id, cam, curFrame))		
				B2 = tde4.removeDistortion2D(cam, curFrame, tde4.getPointPosition2D(curGroup, pointB2_id, cam, curFrame))	
				
				if (((A1[0] - A2[0])*(B1[1] - B2[1])-(A1[1]-A2[1])*(B1[0]-B2[0]))<0.0001 and ((A1[0] - A2[0])*(B1[1] - B2[1])-(A1[1]-A2[1])*(B1[0]-B2[0]))>-0.0001): 
				
					tde4.postQuestionRequester("Info" , "Lines are close to parallel - aborted" , "OK")
					
				else:
				
					#magic happens next (intersection of line given two points equation)
					
					intersectPoint = [((A1[0]*A2[1]-A1[1]*A2[0])*(B1[0]-B2[0])-(A1[0]-A2[0])*(B1[0]*B2[1]-B1[1]*B2[0])) /
					((A1[0] - A2[0])*(B1[1] - B2[1])-(A1[1]-A2[1])*(B1[0]-B2[0])),
					((A1[0]*A2[1]-A1[1]*A2[0])*(B1[1]-B2[1])-(A1[1]-A2[1])*(B1[0]*B2[1]-B1[1]*B2[0])) /
					((A1[0] - A2[0])*(B1[1] - B2[1])-(A1[1]-A2[1])*(B1[0]-B2[0]))]

					#update point position to be redistorted intersection point
					
					tde4.setPointPosition2D(curGroup, curPoint, cam, curFrame, tde4.applyDistortion2D(cam, curFrame, intersectPoint))

update_by_intersect()