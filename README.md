**Intersect Points Readme**

*Overview*

These two scripts are for placing a point at a location which can't be seen for
instance it might be obscured by an object or be out of frame. To do this it requires
two reference points to be placed on each of two lines which intersect at it's location.
 This makes it ideal for tracking corners of objects or grid based patterns.

*Install instructions*

Copy both files (ozols_update_by_isct.py and ozols_create_isct_points.py) into your
3DE scripts directory (py_scripts).

The scripts will appear in the manual tracking window under the IntersectPoints menu.

*How to use*

1. Select the point you want to update with intersect information

2. Use "Create points for intersection" script in the Manual Tracking>Intersect Points menu

3. Place the newly created points on the lines. A1 & A2 on one line and B1 & B2 on the other.

4. Reselect your initial point and use "Update point by intersection" script.


*Notes*

Only the point you want to update must be selected when running the Update by intersection script

If the name of the original point is changed the intersect ref points need to be renamed 
following the same [original point name]_A1 _A2 etc. pattern

It does take into account lens distortion, so make sure your distortion settings are 
configured beforedhand.

It only operates on the current camera and current frame. If you want to do multiple frames
place _A1, _A2, etc. for each frame and then run Update point by intersection per frame.


David Ozols